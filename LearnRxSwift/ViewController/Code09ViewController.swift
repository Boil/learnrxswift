//
//  Code09ViewController.swift
//  LearnRxSwift
//
//  Created by Kystar's Mac Book Pro on 2018/12/26.
//  Copyright © 2018 kystar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

/*
 过滤操作：
    1.amb
        当传入多个 Observables 到 amb 操作符时，它将取第一个发出元素或产生事件的 Observable，然后只发出它的元素。并忽略掉其他的 Observables。
    2.takeWhile
        该方法依次判断 Observable 序列的每一个值是否满足给定的条件。 当第一个不满足条件的值出现时，它便自动完成。
    3.takeUntil
        除了订阅源 Observable 外，通过 takeUntil 方法我们还可以监视另外一个 Observable， 即 notifier。
        如果 notifier 发出值或 complete 通知，那么源 Observable 便自动完成，停止发送事件。
    4.skipWhile
        该方法用于跳过前面所有满足条件的事件。
        一旦遇到不满足条件的事件，之后就不会再跳过了。
    5.skipUntil
        同上面的 takeUntil 一样，skipUntil 除了订阅源 Observable 外，通过 skipUntil 方法我们还可以监视另外一个 Observable， 即 notifier 。
        与 takeUntil 相反的是。源 Observable 序列事件默认会一直跳过，直到 notifier 发出值或 complete 通知。
 */
struct Code09ViewModel {
    let data = Observable.just([
        CellInfoModel(title:"条件和布尔操作符:amb", funcName:"amb"),
        CellInfoModel(title:"条件和布尔操作符:takeWhile", funcName:"takeWhile"),
        CellInfoModel(title:"条件和布尔操作符:takeUntil", funcName:"takeUntil"),
        CellInfoModel(title:"条件和布尔操作符:skipWhile", funcName:"skipWhile"),
        CellInfoModel(title:"条件和布尔操作符:skipUntil", funcName:"skipUntil")
        ])
}

class Code09ViewController: UIViewController {

    lazy var tableView : UITableView = {
        let tableView = UITableView(frame: self.view.bounds, style: .plain)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellReuseID)
        return tableView
    }()
    let disposeBag = DisposeBag()
    let viewModel = Code09ViewModel()
    
    let CellReuseID = "CellReuseID"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(tableView)
        
        viewModel.data.bind(to: tableView.rx.items(cellIdentifier : CellReuseID)) { _ , model, cell in
            cell.textLabel?.text = model.title
            cell.detailTextLabel?.text = model.funcName
        }.disposed(by: disposeBag)
        
        tableView.rx.modelSelected(CellInfoModel.self).subscribe(onNext : { model in
            let aSelec = NSSelectorFromString(model.funcName)
            self.performSelector(inBackground: aSelec, with: nil)
        }).disposed(by: disposeBag)
        
    }
    
    // 只接收第一个信号源的元素，忽略掉其他的信号
    @objc func amb() {
        let subject1 = PublishSubject<Int>()
        let subject2 = PublishSubject<Int>()
        let subject3 = PublishSubject<Int>()
        
        subject1.amb(subject3)
            .amb(subject2)
            .subscribe(onNext: {print($0)})
            .disposed(by: disposeBag)
        
        subject2.onNext(1)  //只发出subject2的信号
        subject1.onNext(20)
        subject2.onNext(2)
        subject1.onNext(40)
        subject3.onNext(0)
        subject2.onNext(3)
        subject1.onNext(60)
        subject3.onNext(0)
        subject3.onNext(0)
    }
    
    //直到不满足条件
    @objc func takeWhile() {
        Observable.of(2, 3, 4, 5, 6)
            .takeWhile {$0 < 4}
            .subscribe(onNext : {print($0)})
            .disposed(by: disposeBag)
    }
    
    @objc func takeUntil() {
        let source = PublishSubject<String>()
        let notifier = PublishSubject<String>()
        
        source
            .takeUntil(notifier)
            .subscribe(onNext : {print($0)})
            .disposed(by: disposeBag)
        
        source.onNext("a")
        source.onNext("b")
        source.onNext("c")
        source.onNext("d")
        
        //停止接收消息
        notifier.onNext("s")    //接收到新消息，不再接收source信号
        
        source.onNext("e")
        source.onNext("f")
        source.onNext("g")
    }
    
    //跳过不符合条件的事件
    @objc func skipWhile() {
        Observable.of(2, 3, 4, 5, 6)
            .skipWhile { $0 < 4}
            .subscribe(onNext : { print($0)})
            .disposed(by: disposeBag)
    }
    
    @objc func skipUntil() {
        let source = PublishSubject<String>()
        let notifier = PublishSubject<String>()
        
        source
            .skipUntil(notifier)
            .subscribe(onNext : { print($0)} )
            .disposed(by: disposeBag)
        
        source.onNext("1")
        source.onNext("2")
        source.onNext("3")
        source.onNext("4")
        
        //开始接收消息
        notifier.onNext("0")
        
        source.onNext("5")
        source.onNext("6")
        
        //仍然接收
        notifier.onNext("00")
        
        source.onNext("7")
        source.onNext("8")
        
    }
}
