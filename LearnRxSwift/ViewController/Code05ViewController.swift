//
//  Code05ViewController.swift
//  LearnRxSwift
//
//  Created by Kystar's Mac Book Pro on 2018/12/26.
//  Copyright © 2018 kystar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SnapKit

/*
 自定义可绑定属性
    方式一：通过对 UI 类进行扩展
    方式二：通过对 Reactive 类进行扩展
 
 RxSwift 自带的可绑定属性（UI 观察者）

 */

struct Code05ViewMode {
    let data = Observable.just([
        CellInfoModel(title: "通过对UI类扩展绑定属性", funcName: "uiExtensions"),
        CellInfoModel(title: "通过对Reactive类扩展绑定属性", funcName: "reactiveExtensions"),
        CellInfoModel(title: "RxSwift 自带的可绑定属性（UI 观察者）", funcName: "rxSwiftBinder"),
    ])
}

class Code05ViewController: UIViewController {

    let CellReuseId = "CellReuseId"
    
    lazy var topLabel : UILabel = {
        let label = UILabel()
        label.text = "字号变化"
        label.backgroundColor = .red
        return label
    }()
    
    lazy var centerLabel : UILabel = {
        let label = UILabel()
        label.text = "字号变化"
        label.backgroundColor = .yellow
        return label
    }()
    
    lazy var bottomLabel : UILabel = {
        let label = UILabel()
        label.backgroundColor = .green
        return label
    }()
    
    lazy var tableView : UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellReuseId)
        return tableView
    }()
    
    let disposeBag = DisposeBag()
    let viewModel = Code05ViewMode()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupSubViews()
        viewModel.data.bind(to: tableView.rx.items(cellIdentifier : CellReuseId)) { _ , model , cell in
            cell.textLabel?.text = model.title
            cell.detailTextLabel?.text = model.funcName
        }.disposed(by: disposeBag)
        
        tableView.rx.modelSelected(CellInfoModel.self).subscribe(onNext: { model in
            let aSelec : Selector = NSSelectorFromString(model.funcName)
            
            self.performSelector(inBackground: aSelec, with: nil)
            
        }).disposed(by: disposeBag)
        
    }

    func setupSubViews() {
        self.view.addSubview(topLabel)
        self.view.addSubview(centerLabel)
        self.view.addSubview(bottomLabel)
        self.view.addSubview(tableView)
        
        topLabel.snp.makeConstraints { (make) in
            if #available(iOS 11.0, *) {
                make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top)
            } else {
                make.top.equalTo(self.topLayoutGuide.snp.bottom)
            }
            make.left.right.equalTo(self.view)
            make.height.equalTo(50)
        }
        
        centerLabel.snp.makeConstraints { (make) in
            make.left.right.height.equalTo(topLabel)
            make.top.equalTo(topLabel.snp.bottom)
        }
        
        bottomLabel.snp.makeConstraints { (make) in
            make.left.right.height.equalTo(topLabel)
            make.top.equalTo(centerLabel.snp.bottom)
        }
        
        tableView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self.view)
            make.top.equalTo(bottomLabel.snp.bottom)
        }
    }
    
    @objc func uiExtensions() {
        //Observable序列
        let observable = Observable<Int>.interval(1, scheduler: MainScheduler.instance)
        observable
            .map { CGFloat($0)}
            .bind(to: topLabel.fontSize)
            .disposed(by: disposeBag)
    }
    
    @objc func reactiveExtensions() {
        let observable = Observable<Int>.interval(1, scheduler: MainScheduler.instance)
        observable
            .map { CGFloat($0)}
            .bind(to: topLabel.rx.fontSize)
            .disposed(by: disposeBag)
        
    }
    
    /* 其实 RxSwift 已经为我们提供许多常用的可绑定属性。比如 UILabel 就有 text 和 attributedText 这两个可绑定属性
     extension Reactive where Base: UILabel {
     
        /// Bindable sink for `text` property.
        public var text: Binder<String?> {
            return Binder(self.base) { label, text in
            label.text = text
        }
     }
     
     /// Bindable sink for `attributedText` property.
     public var attributedText: Binder<NSAttributedString?> {
        return Binder(self.base) { label, text in
            label.attributedText = text
        }
     }
     
     }
    */
    @objc func rxSwiftBinder() {
        //Observable序列（每隔1秒钟发出一个索引数）
        let observable = Observable<Int>.interval(1, scheduler: MainScheduler.instance)
        observable
            .map { "当前索引数：\($0 )"}
            .bind(to: bottomLabel.rx.text) //收到发出的索引数后显示到label上
            .disposed(by: disposeBag)
    }

}

// MARK: - ---- UILabel Extension
extension UILabel {
    var fontSize : Binder<CGFloat> {
        return Binder(self) { label , fontSize in
            label.font = UIFont.systemFont(ofSize: fontSize)
        }
    }
}

// MARK: - ---- Reactive Extension
extension Reactive where Base : UILabel {
    var fontSize : Binder<CGFloat> {
        return Binder(self.base) { label, fontSize in
            label.font = UIFont.systemFont(ofSize: fontSize)
        }
    }
}
