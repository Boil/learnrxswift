//
//  Code03ViewController.swift
//  LearnRxSwift
//
//  Created by Kystar's Mac Book Pro on 2018/12/26.
//  Copyright © 2018 kystar. All rights reserved.
//

import UIKit
import RxSwift
/*
 Observable是信号源，负责发出信号，比如UITextField的text变化，就是一个信号
 有Observable还需要使用subscribe()方法来订阅（监听）它，接收它发出的Event。
 
 subscribe类型：
    subscribe {}
    subscribe(onNext: onError: onCompleted: onDisposed:)
 
 监听事件的生命周期：
    使用doOn来监听事件的声明周期，他会在每一次事件发送前被调用
 
 Observable的销毁（Dispose）
    Observable 从创建到终结流程
        （1）一个 Observable 序列被创建出来后它不会马上就开始被激活从而发出 Event，而是要等到它被某个人订阅了才会激活它。
        （2）而 Observable 序列激活之后要一直等到它发出了 .error 或者 .completed 的 event 后，它才被终结。
 
 */
// MARK: - ---- Model
struct CellInfoModel {
    let title : String
    let funcName : String
}

extension CellInfoModel : CustomStringConvertible {
    var description : String {
        return "name: \(title) funcName: \(funcName)"
    }
}

// MARK: - ---- ViewModel
struct Code03ViewModel {
    let data = Observable.just([
        CellInfoModel(title: "subscribe类型1", funcName: "subscribe1"),
        CellInfoModel(title: "subscribe类型2", funcName: "subscribe2"),
        CellInfoModel(title: "监听事件的生命周期", funcName: "doOn"),
        CellInfoModel(title: "Observable的销毁 dispose()", funcName: "dispose"),
        CellInfoModel(title: "Observable的销毁 DisposeBag", funcName: "useDisposeBag")
        ])
}

// MARK: - ---- ViewController
class Code03ViewController: UIViewController {

    let CellReuseID = "CellReuseId"
    
    let viewModel = Code03ViewModel()
    
    let disposeBag = DisposeBag()
    
    lazy var tableView : UITableView = {
        let tableView = UITableView(frame: self.view.bounds, style: .plain)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellReuseID)
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.addSubview(tableView)
        
        viewModel.data.bind(to: tableView.rx.items(cellIdentifier : CellReuseID)) { _ , model, cell in
            cell.textLabel?.text = model.title
            cell.detailTextLabel?.text = model.funcName
        }.disposed(by: disposeBag)
        
        tableView.rx.modelSelected(CellInfoModel.self).subscribe(onNext: { model in
            
            let aSelec : Selector = NSSelectorFromString(model.funcName)
            
            self.performSelector(inBackground: aSelec, with: nil)
            
        }).disposed(by: disposeBag)
    }

    @objc func subscribe1() {
        let observable = Observable.of("A", "B", "C")
        
        //使用block去订阅
        observable.subscribe {
            print($0)
        }.disposed(by: disposeBag)
        
        observable.subscribe { event in
            print(event)
        }.disposed(by: disposeBag)
    }
    
    @objc func subscribe2() {
        let observable = Observable.of("A", "B", "C")
        
        observable.subscribe(onNext: { element in
            print(element)
        }, onError: { error in
            print(error)
        }, onCompleted: {
            print("completed")
        }, onDisposed: {
            print("disposed")
        }).disposed(by: disposeBag)
        
        // onNext、onError、onCompleted 和 onDisposed 这四个回调 block 参数都是有默认值的，即它们都是可选的。所以我们也可以只处理 onNext 而不管其他的情况。
        observable.subscribe(onNext: { element in
            print(element)
        }).disposed(by: disposeBag)
    }
    
    @objc func doOn() {
        let observable = Observable.of("A", "B", "C")
        
        observable.do(onNext: { element in
            print(element)
        }, onError: { error in
            print(error)
        }, onCompleted: {
            print("completed")
        }, onDispose: { //与subscribe最后一个参数不同  onDispose <vs> onDisposed
            print("disposed")
        })
        .subscribe(onNext: { element in
            print(element)
        }, onError: { error in
            print(error)
        }, onCompleted: {
            print("completed")
        }, onDisposed: {
            print("disposed")
        }).disposed(by: disposeBag)
        
    }
    
    @objc func dispose() {
        let observable = Observable.of("A", "B", "C")
        
        let subscription = observable.subscribe { event in
            print(event)
        }
        
        subscription.dispose()
    }
    
    @objc func useDisposeBag() {
        let disposeBag = DisposeBag()
        
        let observable = Observable.of("A","B", "C")
        observable.subscribe { event in
            print(event)
        }.disposed(by: disposeBag)
        
    }
}
