//
//  Code08ViewController.swift
//  LearnRxSwift
//
//  Created by Kystar's Mac Book Pro on 2018/12/26.
//  Copyright © 2018 kystar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

/*
 过滤操作：
    1.filter
        该操作符就是用来过滤掉某些不符合要求的事件。
    2.distinctUntilChanged
        该操作符用于过滤掉连续重复的事件。
    3.single
        限制只发送一次事件，或者满足条件的第一个事件。
        如果存在有多个事件或者没有事件都会发出一个 error 事件。
        如果只有一个事件，则不会发出 error 事件。
    4.elementAt
        该方法实现只处理在指定位置的事件。
    5.ignoreElements
        该操作符可以忽略掉所有的元素，只发出 error 或 completed 事件。
        如果我们并不关心 Observable 的任何元素，只想知道 Observable 在什么时候终止，那就可以使用 ignoreElements 操作符。
    6.take
        该方法实现仅发送 Observable 序列中的前 n 个事件，在满足数量之后会自动 .completed。
    7.takeLast
        该方法实现仅发送 Observable 序列中的后 n 个事件。
    8.skip
        该方法用于跳过源 Observable 序列发出的前 n 个事件。
    9.Sample
        Sample 除了订阅源 Observable 外，还可以监视另外一个 Observable， 即 notifier 。
        每当收到 notifier 事件，就会从源序列取一个最新的事件并发送。而如果两次 notifier 事件之间没有源序列的事件，则不发送值。
    10.debounce
        debounce 操作符可以用来过滤掉高频产生的元素，它只会发出这种元素：该元素产生后，一段时间内没有新元素产生。
        换句话说就是，队列中的元素如果和下一个元素的间隔小于了指定的时间间隔，那么这个元素将被过滤掉。
        debounce 常用在用户输入的时候，不需要每个字母敲进去都发送一个事件，而是稍等一下取最后一个事件。
 
 */

struct Code08ViewModel {
    let data = Observable.just([
        CellInfoModel(title: "过滤操作符：filter" , funcName : "filter"),
        CellInfoModel(title: "过滤操作符：distinctUntilChanged" , funcName : "distinctUntilChanged"),
        CellInfoModel(title: "过滤操作符：single" , funcName : "single"),
        CellInfoModel(title: "过滤操作符：elementAt" , funcName : "elementAt"),
        CellInfoModel(title: "过滤操作符：ignoreElements" , funcName : "ignoreElements"),
        CellInfoModel(title: "过滤操作符：take" , funcName : "take"),
        CellInfoModel(title: "过滤操作符：takeLast" , funcName : "takeLast"),
        CellInfoModel(title: "过滤操作符：skip" , funcName : "skip"),
        CellInfoModel(title: "过滤操作符：Sample" , funcName : "sample"),
        CellInfoModel(title: "过滤操作符：debounce" , funcName : "debounce"),
        ])
}

class Code08ViewController: UIViewController {

    let CellReuseID = "CellReuseID"
    let viewModel = Code08ViewModel()
    lazy var tableView : UITableView = {
        let tableView = UITableView.init(frame: self.view.bounds, style: .plain)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellReuseID)
        return tableView
    }()
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.view.addSubview(tableView)
        
        viewModel.data.bind(to: tableView.rx.items(cellIdentifier : CellReuseID)) { _, model , cell in
            cell.textLabel?.text = model.title
            cell.detailTextLabel?.text = model.funcName
        }.disposed(by: disposeBag)

        tableView.rx.modelSelected(CellInfoModel.self).subscribe(onNext : { model in
            let aSelec = NSSelectorFromString(model.funcName)
            self.performSelector(inBackground: aSelec, with: nil)
            
        }).disposed(by: disposeBag)
    }
    
    //按条件筛选过滤
    @objc func filter() {
        Observable.of(2, 30 , 22 , 5, 60, 3, 40 ,9 , 20)
            .filter { $0 > 20}  //过滤掉小于20的数
            .subscribe(onNext : { print($0)})
            .disposed(by: disposeBag)
    }
    
    //前后数据不一样则会发出信号，相同不发
    @objc func distinctUntilChanged() {
        Observable.of(1, 2, 3, 1, 1, 1, 4) //重复发了3个1，只有一个1会发出来，后面两个1不发
            .distinctUntilChanged()
            .subscribe(onNext: {print($0)})
            .disposed(by: disposeBag)
    }
    
    //只发出满足条件的第一个事件，如果有多个事件或者没有事件都会发出Error事件
    @objc func single() {
        Observable.of(1, 2, 3, 4)
            .single {$0 == 2}
            .subscribe(onNext: {print($0)})
            .disposed(by: disposeBag)
        
        Observable.of("A", "B", "C", "D")
            .single()
            .subscribe(onNext: {print($0)})
            .disposed(by: disposeBag)
    }
    
    //发出指定位置的事件
    @objc func elementAt() {
        Observable.of(1, 2, 3, 4)
            .elementAt(2)
            .subscribe(onNext: {print($0)})
            .disposed(by: disposeBag)
    }
    
    @objc func ignoreElements() {
        Observable.of(1,2 ,3,4)
            .ignoreElements()
            .subscribe { print($0)}
            .disposed(by: disposeBag)
    }
    
    @objc func take() {
        Observable.of(1,2,3,4)
            .take(2)
            .subscribe(onNext : {print($0)})
            .disposed(by: disposeBag)
    }
    
    @objc func takeLast() {
        Observable.of(1,2,3,4)
            .takeLast(1)
            .subscribe(onNext : {print($0)})
            .disposed(by: disposeBag)
    }
    
    @objc func skip() {
        Observable.of(1,2,3,4)
            .skip(2)
            .subscribe(onNext : {print($0)})
            .disposed(by: disposeBag)
    }
    
    @objc func sample() {
        let source = PublishSubject<Int>()
        let notifier = PublishSubject<String>()
        
        source.sample(notifier)
            .subscribe(onNext : {print($0)})
            .disposed(by: disposeBag)
        
        source.onNext(1)
        
        notifier.onNext("A")
        
        source.onNext(2)
        
        notifier.onNext("B")
        
        notifier.onNext("C")
        
        source.onNext(3)
        
        source.onNext(4)
        
        notifier.onNext("D")
        
        source.onNext(5)
        
        notifier.onCompleted()
    }
    
    @objc func debounce() {
        let times = [
            ["value" : 1, "time" : 0.1],
            ["value" : 2, "time" : 1.1],
            ["value" : 3, "time" : 1.2],
            ["value" : 4, "time" : 1.2],
            ["value" : 5, "time" : 1.5],
            ["value" : 6, "time" : 1.7],
            ["value" : 7, "time" : 2.1],
            ["value" : 8, "time" : 2.6000001],
        ]
        
        Observable.from(times)
            .flatMap{ item in
                return Observable.of(Int(item["value"]!))
                            .delaySubscription(Double(item["time"]!), scheduler: MainScheduler.instance)
        }
        .debounce(0.5, scheduler: MainScheduler.instance)//只发出与下一个间隔超过0.5秒的元素
            .subscribe(onNext : {print($0)})
            .disposed(by: disposeBag)
    }
}
