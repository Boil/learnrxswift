//
//  Code06ViewController.swift
//  LearnRxSwift
//
//  Created by Kystar's Mac Book Pro on 2018/12/26.
//  Copyright © 2018 kystar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

/*
 动态产生与捕获信号
 
    Subjects
        1、Subjects 既是订阅者，也是 Observable， 既能产生信号，也能接收信号
        2、四种 Subjects:
            1）、PublishSubject、BehaviorSubject、ReplaySubject、Variable
            2）、各自的特点，也有相同之处：
                a、首先他们都是 Observable，他们的订阅者都能收到他们发出的新的 Event。
                b、直到 Subject 发出 .complete 或者 .error 的 Event 后，该 Subject 便终结了，同时它也就不会再发出 .next 事件。
                c、对于那些在 Subject 终结后再订阅他的订阅者，也能收到 subject 发出的一条 .complete 或 .error 的 event，告诉这个新的订阅者它已经终结了。
                d、他们之间最大的区别只是在于：当一个新的订阅者刚订阅它的时候，能不能收到 Subject 以前发出过的旧 Event，如果能的话又能收到多少个。
 
    1、PublishSubject
            PublishSubject 是最普通的 Subject，它不需要初始值就能创建。
            PublishSubject 的订阅者从他们开始订阅的时间点起，可以收到订阅后 Subject 发出的新 Event，而不会收到他们在订阅前已发出的 Event。
 
    2、BehaviorSubject
            需要通过一个默认初始值来创建。
            当一个订阅者来订阅它的时候，这个订阅者会立即收到 BehaviorSubjects 上一个发出的 event。之后就跟正常的情况一样，它也会接收到 BehaviorSubject 之后发出的新的 event。

    3、ReplaySubject
            ReplaySubject 在创建时候需要设置一个 bufferSize，表示它对于它发送过的 event 的缓存个数。
            比如一个 ReplaySubject 的 bufferSize 设置为 2，它发出了 3 个 .next 的 event，那么它会将后两个（最近的两个）event 给缓存起来。此时如果有一个 subscriber 订阅了这个 ReplaySubject，那么这个 subscriber 就会立即收到前面缓存的两个 .next 的 event。
            如果一个 subscriber 订阅已经结束的 ReplaySubject，除了会收到缓存的 .next 的 event 外，还会收到那个终结的 .error 或者 .complete 的 event。

    4、Variable
        （注意：由于 Variable 在之后版本中将被废弃，建议使用 Varible 的地方都改用下面介绍的 BehaviorRelay 作为替代。）
        Variable 其实就是对 BehaviorSubject 的封装，所以它也必须要通过一个默认的初始值进行创建。
        Variable 具有 BehaviorSubject 的功能，能够向它的订阅者发出上一个 event 以及之后新创建的 event。
        不同的是，Variable 还把会把当前发出的值保存为自己的状态。同时它会在销毁时自动发送 .complete 的 event，不需要也不能手动给 Variables 发送 completed 或者 error 事件来结束它。
        简单地说就是 Variable 有一个 value 属性，我们改变这个 value 属性的值就相当于调用一般 Subjects 的 onNext() 方法，而这个最新的 onNext() 的值就被保存在 value 属性里了，直到我们再次修改它。
        注意：
            Variables 本身没有 subscribe() 方法，但是所有 Subjects 都有一个 asObservable() 方法。我们可以使用这个方法返回这个 Variable 的 Observable 类型，拿到这个 Observable 类型我们就能订阅它了。
 
    5、BehaviorRelay
        BehaviorRelay 是作为 Variable 的替代者出现的。它的本质其实也是对 BehaviorSubject 的封装，所以它也必须要通过一个默认的初始值进行创建。
        BehaviorRelay 具有 BehaviorSubject 的功能，能够向它的订阅者发出上一个 event 以及之后新创建的 event。
        与 BehaviorSubject 不同的是，不需要也不能手动给 BehaviorReply 发送 completed 或者 error 事件来结束它（BehaviorRelay 会在销毁时也不会自动发送 .complete 的 event）。
        BehaviorRelay 有一个 value 属性，我们通过这个属性可以获取最新值。而通过它的 accept() 方法可以对值进行修改。
 */

struct Code06ViewModel {
    let data = Observable.just([
        CellInfoModel(title: "PublishSubject", funcName:"publishSubject"),
        CellInfoModel(title: "BehaviorSubject", funcName:"behaviorSubject"),
        CellInfoModel(title: "ReplaySubject", funcName:"replaySubject"),
        CellInfoModel(title: "Variable", funcName:"variable"),
        CellInfoModel(title: "behaviorRelay", funcName:"behaviorRelay"),
        CellInfoModel(title: "BehaviorRelayAppendValue", funcName:"behaviorRelayAppendValue"),
        
        ])
}

class Code06ViewController: UIViewController {
    
    let CellReuseId = "CellReuseId"
    
    lazy var tableView : UITableView = {
        let tableView = UITableView(frame: self.view.bounds, style: .plain)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellReuseId)
        
        return tableView
    }()

    let disposeBag = DisposeBag()
    
    let viewModel = Code06ViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.addSubview(tableView)
        
        viewModel.data.bind(to: tableView.rx.items(cellIdentifier : CellReuseId)) { _, model , cell in
            cell.textLabel?.text = model.title
            cell.detailTextLabel?.text = model.funcName
        }.disposed(by: disposeBag)
        
        tableView.rx.modelSelected(CellInfoModel.self).subscribe(onNext: {  model in
            let aSelec = NSSelectorFromString(model.funcName)
            self.performSelector(inBackground: aSelec, with: nil)
        }).disposed(by: disposeBag)
    }
    
    //不管subject有没有completeed，订阅的时候，最少能收到一个completed信号
    @objc func publishSubject() {
        //创建一个PublishSubject
        let publishSubject = PublishSubject<String>()
        
        //由于当前没有任何订阅者，所以这条信息不会输出到控制台
        publishSubject.onNext("111")
        
        //第一次订阅subject
        publishSubject.subscribe(onNext: { string in
            print("第1次订阅",string)
        }, onCompleted: {
            print("第1次订阅 onCompleted")
        }).disposed(by: disposeBag)
        
        //当前有一个订阅，则该信息会输出到控制台
        publishSubject.onNext("2222")
        
        // 第2次订阅subject
        publishSubject.subscribe(onNext: { string in
            print("第2次订阅",string)
        }, onCompleted: {
            print("第2次订阅 onCompleted")
        }).disposed(by: disposeBag)
        
        //当前有2个订阅，则该信息会输出到控制台
        publishSubject.onNext("33333")
        
        // 让subject结束，发送onCompleted以后，发送onNext信息，订阅者不会收到，但是新订阅者和以前的订阅者都可以收到一次onCompleted信号
        publishSubject.onCompleted()
        
        // subject完成后会发出.next事件了
        publishSubject.onNext("444444")
        
        // subject完成后他的所有订阅（包括结束后的订阅），都能收到subject的.completed事件
        publishSubject.subscribe(onNext: { string in
            print("第3次订阅",string)
        }, onCompleted: {
            print("第3次订阅 onCompleted")
        }).disposed(by: disposeBag)
    }
    
    //没completed的时候，订阅能立即收到上一个信号
    @objc func behaviorSubject() {
        // 创建一个BehaviorSubject
        let behaviorSubject = BehaviorSubject(value: "11")
        
        // 第一次订阅，先收到上一个
        behaviorSubject.subscribe { event in
            print("第1次订阅", event)
            
        }.disposed(by: disposeBag)
        
        
        //发送next事件
        behaviorSubject.onNext("222")
        
        // 发送error事件
        behaviorSubject.onError(NSError(domain: "com.xby", code: 0, userInfo: nil))
        
        //第二次订阅，先收到上一个
        behaviorSubject.subscribe { event in
            print("第2次订阅",event)
            
        }.disposed(by: disposeBag)
        
    }
    
    // bufferSize表示缓存event的数量，缓存几个，订阅的时候，这几个都会先打印一次
    @objc func replaySubject() {
        // 创建一个缓存两个event的subject
        let replaySubject = ReplaySubject<String>.create(bufferSize: 2)
        
        //发送三个信号
        replaySubject.onNext("111")
        replaySubject.onNext("222")
        replaySubject.onNext("333")
        
        // 第一次订阅，打印222，333，因为只能缓存2个
        replaySubject.subscribe { event in
            print("第1次订阅",event)
        }.disposed(by: disposeBag)
        
        // 再发送一个信号
        replaySubject.onNext("444") //上面的第一次订阅会正常接收
        
        // 第二次订阅
        replaySubject.subscribe { event in
            print("第2次订阅",event)    //打印333，444
        }.disposed(by: disposeBag)
        
        //让subject结束
        replaySubject.onCompleted()
        
        //第三次订阅，444， completed
        replaySubject.subscribe { event in
            print("第3次订阅",event)
        }.disposed(by: disposeBag)
    }
    
    //对BehaviorSubject的封装，当这个方法执行完毕后，这个 Variable 对象就会被销毁，同时它也就自动地向它的所有订阅者发出 .completed 事件
    @objc func variable() {
        //初始化的值
        let variable = Variable("111")
        
        //修改value的值
        variable.value = "222"
        
        // 第一次订阅
        variable.asObservable().subscribe {
            print("第1次订阅",$0)
        }.disposed(by: disposeBag)
        
        // 修改value的值
        variable.value = "333"
        
        // 第二次订阅
        variable.asObservable().subscribe {
            print("第2次订阅",$0)
        }.disposed(by: disposeBag)
        
        //修改value的值
        variable.value = "444"
        
        //自动启动completed事件
        
    }
    
    @objc func behaviorRelay() {
        //创建一个初始值为111的subject
        let subject = BehaviorRelay<String>(value: "111")
        
        //修改value
        subject.accept("222")
        
        //第一次订阅
        subject.asObservable().subscribe {
            print("第1次订阅",$0)
        }.disposed(by: disposeBag)
        
        //修改value的值
        subject.accept("333")
        
        subject.asObservable().subscribe {
            print("第2次订阅",$0)
        }.disposed(by: disposeBag)
    
        //修改value的值
        subject.accept("444")
        
        //不会自动发送completed，也不能手动发送
    }
    
    //拼接上一个原有值
    @objc func behaviorRelayAppendValue() {
        //初始化一个数组
        let subject = BehaviorRelay<[String]>(value: ["1"])
        
        //修改value的值
        subject.accept(subject.value + ["2", "3"])
        
        //第一次订阅
        subject.asObservable().subscribe {
            print("第1次订阅",$0)
        }.disposed(by: disposeBag)
        
        //修改value的值
        subject.accept(subject.value + ["4","5"])
        
        //第2次订阅
        subject.asObservable().subscribe {
            print("第2次订阅",$0)
        }.disposed(by: disposeBag)
        
        //修改value值
        subject.accept(subject.value + ["6", "7"])
    }
}
