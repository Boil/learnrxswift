//
//  RootViewController.swift
//  HGRxSwift
//
//  Created by Kystar's Mac Book Pro on 2018/12/25.
//  Copyright © 2018 kystar. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

// MARK: - ---- PageInfo
struct PageInfo {
    let pageTitle : String
    public let fileName : String
}

extension PageInfo : CustomStringConvertible {
    var description : String {
        return "name: \(pageTitle) fileName: \(fileName)"
    }
}

// MARK: - ---- ViewModel
struct RootVCViewModel {
    let data = Observable.just([
        PageInfo(pageTitle: "详解2（响应式编程与传统式编程的比较样例）", fileName: "Code01ViewController"),
        PageInfo(pageTitle: "详解3（Observable介绍、创建可观察序列）", fileName: "Code02ViewController"),
        PageInfo(pageTitle: "详解4（Observable订阅、事件监听、订阅销毁）", fileName: "Code03ViewController"),
        PageInfo(pageTitle: "详解5（观察者1： AnyObserver、Binder）", fileName: "Code04ViewController"),
        PageInfo(pageTitle: "详解6（观察者2： 自定义可绑定属性）", fileName: "Code05ViewController"),
        PageInfo(pageTitle: "详解7（Subjects、Variables）", fileName: "Code06ViewController"),
        PageInfo(pageTitle: "详解8（变换操作符：buffer、map、flatMap、scan等）", fileName: "Code07ViewController"),
        PageInfo(pageTitle: "详解9（过滤操作符：filter、take、skip等）", fileName: "Code08ViewController"),
        PageInfo(pageTitle: "详解10（条件和布尔操作符：amb、takeWhile、skipWhile等）", fileName: "Code09ViewController"),
        PageInfo(pageTitle: "详解11（结合操作符：startWith、merge、zip等）", fileName: "Code10ViewController"),

        
        ])
}

// MARK: - ---- RootViewController
class RootViewController: UIViewController {

    lazy var tableView : UITableView = {
        let tableView = UITableView(frame: self.view.bounds, style: .plain)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "PageInoCell")
        return tableView
    }()
    
    // 数据源
    let pageInfoViewModel = RootVCViewModel()
    
    //负责销毁对象
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.view.addSubview(tableView)
        
        // Do any additional setup after loading the view.
        pageInfoViewModel.data
            .bind(to: tableView.rx.items(cellIdentifier: "PageInoCell")) { _ , pageInfo , cell in
                cell.textLabel?.text = pageInfo.pageTitle
                cell.detailTextLabel?.text = pageInfo.fileName
            }.disposed(by: disposeBag)
        
        tableView.rx.modelSelected(PageInfo.self).subscribe(onNext: { pageInfo in
            print("进入【\(pageInfo)】")

            //1:动态获取命名空间
            guard let nameSpace = Bundle.main.infoDictionary!["CFBundleExecutable"] as? String else {
                print("获取命名空间失败")
                return
            }
            let vcClass: AnyClass? = NSClassFromString(nameSpace + "." + pageInfo.fileName) //VCName:表示试图控制器的类名
            // Swift中如果想通过一个Class来创建一个对象, 必须告诉系统这个Class的确切类型
            guard let typeClass = vcClass as? UIViewController.Type else {
                print("vcClass不能当做UIViewController")
                return
            }
            let myVC = typeClass.init()
            //或者加载xib;   let myVC = typeClass.init(nibName: name, bundle: nil)
            self.navigationController?.pushViewController(myVC, animated: true)
        }).disposed(by: disposeBag)
        
    }
}

