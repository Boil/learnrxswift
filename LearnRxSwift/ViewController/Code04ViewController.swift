//
//  Code04ViewController.swift
//  LearnRxSwift
//
//  Created by Kystar's Mac Book Pro on 2018/12/26.
//  Copyright © 2018 kystar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

/*
 观察者（Observer）的作用就是监听事件，然后对这个事件做出响应。或者说任何响应事件的行为都是观察者
 
 创建方式：
    1、直接在 subscribe、bind 方法中创建观察者
        1）、在subscribe 方法中创建观察者
        2）、在bind 方法中创建
    2、使用 AnyObserver 创建观察者
        1）、配合 subscribe 方法使用
        2）、配合 bindTo 方法使用
    3、使用Binder穿件观察者
        基本介绍
        （1）相较于 AnyObserver 的大而全，Binder 更专注于特定的场景。Binder 主要有以下两个特征：
                不会处理错误事件
                确保绑定都是在给定 Scheduler 上执行（默认 MainScheduler）
        （2）一旦产生错误事件，在调试环境下将执行 fatalError，在发布环境下将打印错误信息。
 */

// MARK: - ---- ViewModel
struct Code04ViewModel {
    let data = Observable.just([
        CellInfoModel(title: "在subscribe 方法中创建观察者", funcName: "bySubscribe"),
        CellInfoModel(title: "在bind 方法中创建", funcName: "byBind"),
        CellInfoModel(title: "配合 subscribe 方法使用", funcName: "baseOnSubscribe"),
        CellInfoModel(title: "配合 bindTo 方法使用", funcName: "baseOnBindTo"),
        CellInfoModel(title: "配合 bindTo 方法使用", funcName: "byBinder"),
        
        ])
}

class Code04ViewController: UIViewController {

    let CellReuseId = "CellReuseId"
    lazy var leftLabel : UILabel = {
        let label = UILabel()
        label.backgroundColor = .red
        return label
    }()
    lazy var rightLabel : UILabel = {
        let label = UILabel()
        label.backgroundColor = .green
        return label
    }()
    
    lazy var centerLabel : UILabel = {
        let label = UILabel()
        label.backgroundColor = .yellow
        return label
    }()
    
    lazy var tableView : UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellReuseId)
        return tableView
    }()
    
    let viewModel = Code04ViewModel()
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubViews()

        viewModel.data.bind(to: tableView.rx.items(cellIdentifier: CellReuseId)) { _ , model, cell in
            cell.textLabel?.text = model.title
            cell.detailTextLabel?.text = model.funcName
        }.disposed(by: disposeBag)
        
        tableView.rx.modelSelected(CellInfoModel.self).subscribe(onNext: { model in
            let aSelec : Selector = NSSelectorFromString(model.funcName)
            
            self.performSelector(inBackground: aSelec, with: nil)
        }).disposed(by: disposeBag)
    }
    
    func setupSubViews() {
        self.view.addSubview(leftLabel)
        self.view.addSubview(rightLabel)
        self.view.addSubview(tableView)
        self.view.addSubview(centerLabel)
        
        leftLabel.snp.makeConstraints { (make) in
            if #available(iOS 11.0, *) {
                make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top)
            } else {
                make.top.equalTo(self.topLayoutGuide.snp.bottom)
            }
            make.left.equalTo(self.view)
            make.right.equalTo(self.view.snp.centerX)
            make.height.equalTo(50)
        }
        rightLabel.snp.makeConstraints { (make) in
            make.top.bottom.equalTo(leftLabel)
            make.right.equalTo(self.view)
            make.left.equalTo(self.view.snp.centerX)
        }
        
        centerLabel.snp.makeConstraints { (make) in
            make.top.equalTo(leftLabel.snp.bottom)
            make.left.height.equalTo(leftLabel)
            make.right.equalTo(rightLabel)
        }
        
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(centerLabel.snp.bottom)
            make.left.right.bottom.equalTo(self.view)
        }
    }
    
    @objc func clickAction(button : UIButton) {
        
    }
    
    /// 创建观察者最直接的方法就是在 Observable 的 subscribe 方法后面描述当事件发生时，需要如何做出响应。
    @objc func bySubscribe() {
        
        let observable = Observable.of("A", "B", "C")
        
        observable.subscribe(onNext: { element in
            print(element)
        }).disposed(by: disposeBag)
    }
    
    @objc func byBind() {
        let observable = Observable<Int>.interval(1, scheduler: MainScheduler.instance)
//        observable
//            .map { "当前索引数：\($0)"}
//            .bind { [weak self](text) in
//                self?.leftLabel.text = text
//            }
//            .disposed(by: disposeBag)
        
        // 两种写法等价
        
        observable
            .map { "当前索引数：\($0)"}
            .bind(to : leftLabel.rx.text)
            .disposed(by: disposeBag)
    }
    
    @objc func baseOnSubscribe() {
        // 订阅者
        let observer : AnyObserver<String> = AnyObserver { event in
            switch event {
            case .next(let data):
                print(data)
            case .error(let error):
                print(error)
            case .completed:
                print("complete")
            }
        }
        
        let observable = Observable.of("A", "B", "C")
        observable.subscribe(observer).disposed(by: disposeBag)
    }
    
    @objc func baseOnBindTo() {
        let observer : AnyObserver<String> = AnyObserver { [weak self] event in
            switch event {
            case .next(let text):
                self?.rightLabel.text = text
            default:
                break
            }
        }
        
        let observable = Observable<Int>.interval(1, scheduler: MainScheduler.instance)
        observable.map { "当前索引：\($0)"}
            .bind(to: observer)
            .disposed(by: disposeBag)
    }
    
    @objc func byBinder() {
        // 观察者
        let observer : Binder<String> = Binder(centerLabel) { (view, text) in
            view.text = text
        }
        
        // Observable序列
        let observable = Observable<Int>.interval(1, scheduler: MainScheduler.instance)
        observable
            .map { "当前索引数\($0)"}
            .bind(to: observer)
            .disposed(by: disposeBag)
    }
    
}
