//
//  Code01ViewController.swift
//  HGRxSwift
//
//  Created by Kystar's Mac Book Pro on 2018/12/25.
//  Copyright © 2018 kystar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

// MARK: - ---- Music
struct Music {
    let name : String
    let singer : String
}

extension Music : CustomStringConvertible {
    var description : String {
        return "name: + \(name) singer: \(singer)"
    }
}

// MARK: - ---- MusicLisetViewModel
struct MusicListViewModel {
    let data = Observable.just([
        Music(name: "爱你", singer: "无名"),
        Music(name: "你曾是少年", singer: "S.H.E"),
        Music(name: "从前的我", singer: "陈洁仪"),
        Music(name: "在木星", singer: "朴树"),
        ])
}

// MARK: - ---- ViewController
class Code01ViewController: UIViewController {
    let ReuseID : String = "TableViewCell"
    
    lazy var tableView: UITableView! = {
        let tableView = UITableView(frame: self.view.bounds, style: .plain)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: ReuseID)
        return tableView
    }()
    
    let musicListViewModel = MusicListViewModel()
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(tableView)

        musicListViewModel.data.bind(to: tableView.rx.items(cellIdentifier: ReuseID)) { _ , music , cell in
            cell.textLabel?.text = music.name
            cell.detailTextLabel?.text = music.singer
        }.disposed(by: disposeBag)
        
        tableView.rx.modelSelected(Music.self).subscribe(onNext: { music in
            print("\(music)")
        }).disposed(by: disposeBag)
    }
    
}
