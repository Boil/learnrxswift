//
//  Code02ViewController.swift
//  HGRxSwift
//
//  Created by Kystar's Mac Book Pro on 2018/12/26.
//  Copyright © 2018 kystar. All rights reserved.
//

import UIKit
import RxSwift
/*
Observable<T>
Observable<T> 这个类就是 Rx 框架的基础，我们可以称它为可观察序列。

Observable<T> 的三种事件：
    next：next 事件就是那个可以携带数据 <T> 的事件，可以说它就是一个“最正常”的事件。
    error：error 事件表示一个错误，它可以携带具体的错误内容，一旦 Observable 发出了 error event，则这个 Observable 就等于终止了，以后它再也不会发出 event 事件了。
    completed：completed 事件表示 Observable 发出的事件正常地结束了，跟 error 一样，一旦 Observable 发出了 completed event，则这个 Observable 就等于终止了，以后它再也不会发出 event 事件了。

创建 Observable 序列：
    just()          方法: 该方法通过传入一个默认值来初始化。
    of()            方法: 该方法可以接受可变数量的参数（必需要是同类型的）
    from()          方法: 该方法需要一个数组参数。
    empty()         方法: 该方法创建一个空内容的 Observable 序列。
    never()         方法: 该方法创建一个永远不会发出 Event（也不会终止）的 Observable 序列。
    error()         方法: 该方法创建一个不做任何操作，而是直接发送一个错误的 Observable 序列。
    range()         方法: 该方法通过指定起始和结束数值，创建一个以这个范围内所有值作为初始值的 Observable 序列。
    repeatElement() 方法: 该方法创建一个可以无限发出给定元素的 Event 的 Observable 序列（永不终止）。
    generate()      方法: 该方法创建一个只有当提供的所有的判断条件都为 true 的时候，才会给出动作的 Observable 序列。
    create()        方法: 该方法接受一个 block 形式的参数，任务是对每一个过来的订阅进行处理。
    deferred()      方法: 该个方法相当于是创建一个 Observable 工厂，通过传入一个 block 来执行延迟 Observable 序列创建的行为，而这个 block 里就是真正的实例化序列对象的地方。
    interval()      方法: 这个方法创建的 Observable 序列每隔一段设定的时间，会发出一个索引数的元素。而且它会一直发送下去。
    timer()         方法: 这个方法有两种用法，一种是创建的 Observable 序列在经过设定的一段时间后，产生唯一的一个元素。
*/

// MARK: - ---- Error
enum MyError : Error {
    case A
    case B
}

// MARK: - ---- Model
struct Code02Model {
    let funcName : String
}

// MARK: - ---- ViewModel
struct Code02ViewModel {
    let data = Observable.just([
        Code02Model(funcName: "just"),
        Code02Model(funcName: "of"),
        Code02Model(funcName: "from"),
        Code02Model(funcName: "empty"),
        Code02Model(funcName: "never"),
        Code02Model(funcName: "error"),
        Code02Model(funcName: "range"),
        Code02Model(funcName: "repeatElement"),
        Code02Model(funcName: "generate"),
        Code02Model(funcName: "create"),
        Code02Model(funcName: "deferred"),
        Code02Model(funcName: "interval"),
        Code02Model(funcName: "timer"),
        ])
}

// MARK: - ---- ViewController
class Code02ViewController: UIViewController {

    let viewModel = Code02ViewModel()
    
    let CellReusetID = "CellReusetID"
    
    //负责销毁对象
    let disposeBag = DisposeBag()
    
    
    lazy var tableView : UITableView = {
        let tableView = UITableView(frame: self.view.bounds, style: .plain)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellReusetID)
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addSubview(tableView)
        
        viewModel.data.bind(to: tableView.rx.items(cellIdentifier: CellReusetID)) { _ , mode, cell in
            
            cell.textLabel?.text = mode.funcName
            
        }.disposed(by: disposeBag)
        
        tableView.rx.modelSelected(Code02Model.self).subscribe(onNext: { model in
            
            let aSelec : Selector = NSSelectorFromString(model.funcName)
            
            self.performSelector(inBackground: aSelec, with: nil)
            
        }).disposed(by: disposeBag)
    }
    
    @objc func just() {
        let observable = Observable<Int>.just(5)
        
        observable.subscribe {
            print( $0)
        }.disposed(by: DisposeBag())
        
    }
    
    @objc func of() {
        let observable = Observable.of("A", "B", "C") //需要传入同类型
        
        observable.subscribe {
            print( $0)
        }.disposed(by: DisposeBag())
    }
    
    @objc func from() {
        let observable = Observable.from(["A", "B", "C"])    //与上面of方法作用相同
        observable.subscribe {
            print( $0)
        }.disposed(by: DisposeBag())
    }
    
    @objc func empty() {
        let observable = Observable<Int>.empty()
        observable.subscribe {
            print( $0)
        }.disposed(by: DisposeBag())
    }
    
    /// 永远不会发出Event也不会停止的Observable序列
    @objc func never() {
        let observable = Observable<Int>.never()
        observable.subscribe {
            print( $0)
        }.disposed(by: DisposeBag())
    }
    
    /// 不做任何操作，而是直接发送一个错误的Observable序列
    @objc func error() {
        let observable = Observable<Int>.error(MyError.A)
        observable.subscribe {
            print( $0)
        }.disposed(by: DisposeBag())
    }
    
    /// 已一个范围初始化Observable序列
    @objc func range() {
        //两种写法等价
        let observable = Observable.range(start: 1, count: 5)
    
        let observable2 = Observable.of(1, 2, 3, 4, 5)
        
        observable.subscribe {
            print( $0)
        }.disposed(by: DisposeBag())
        
        observable2.subscribe {
            print( $0)
        }.disposed(by: DisposeBag())
    }
    
    /// 创建一个无限发出给定元素的Event的Observable序列（永不终止）
    @objc func repeatElement() {
        let observable = Observable.repeatElement(1)
        
        observable.subscribe {
            print( $0)
        }.disposed(by: DisposeBag())
    }
    
    @objc func generate() {
        //两种写法等价
        let observable = Observable.generate(initialState: 0,
                                    condition: { $0 <= 10 },    //判定条件
                                    iterate: { $0 + 2 })        //迭代器
        let observable2 = Observable.of(0, 2, 4, 6, 8, 10)
        
        observable.subscribe {
            print( $0)
        }.disposed(by: DisposeBag())
        
        observable2.subscribe {
            print( $0)
        }.disposed(by: DisposeBag())
    }
    
    @objc func create() {
        // 这个block有一个回调参数observer就是订阅这个Observable对象的订阅者
        // 当一个订阅者订阅这个Observable对象的时候，就会将订阅者作为参数传入这个block来执行一些内容
        let  observable =  Observable<String>.create { observer in
            
            // 对订阅者发出了.next事件，并且携带了相关数据“Lean About RxSwif”
            observer.onNext("Lean About RxSwift")
            
            // 对订阅者发出了.completed事件
            observer.onCompleted()
            
            //因为一个订阅行为会有一个Disposable类型的返回值，所以结尾一定要return一个Disposable
            return Disposables.create()
        }
        
        observable.subscribe {
            print( $0)
        }.disposed(by: DisposeBag())
    }
    
    @objc func deferred() {
        // 标记奇数还是偶数
        var isOdd = true
        
        //使用deferred()方法延迟Observable序列的初始化，通过传入的block来实现Observable序列的初始化并返回
        let factory : Observable<Int> = Observable.deferred {
            // 奇偶交替
            isOdd = !isOdd
            
            if isOdd {
                return Observable.of(1, 3, 5, 7)
            } else {
                return Observable.of(2, 4, 6, 8)
            }
        }
        
        // 第一次订阅测试
        factory.subscribe { event in
            print("\(isOdd)", event)
            
        }.disposed(by: DisposeBag())
        
        // 第二次订阅测试
        factory.subscribe { event in
            print("\(isOdd)", event)
        }.disposed(by: DisposeBag())
    }
    
    /// 其每 1 秒发送一次，并且是在主线程（MainScheduler）发送。
    @objc func interval() {
        let observable = Observable<Int>.interval(1, scheduler: MainScheduler.instance)
        
        observable.subscribe { event in
            print(event)
        }.disposed(by: disposeBag)
    }
    
    @objc func timer() {
        //5秒种后发出唯一的一个元素0
        let observable = Observable<Int>.timer(5, scheduler: MainScheduler.instance)
        
        observable.subscribe { event in
            print(event)
        }.disposed(by: disposeBag)
        
        //延时5秒种后，每隔1秒钟发出一个元素
        let observable2 = Observable<Int>.timer(5, period: 1, scheduler: MainScheduler.instance)
        observable2.subscribe { event in
            print(event)
        }.disposed(by: disposeBag)
    }
}
