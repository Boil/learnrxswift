//
//  Code07ViewController.swift
//  LearnRxSwift
//
//  Created by Kystar's Mac Book Pro on 2018/12/26.
//  Copyright © 2018 kystar. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

/*
 变换操作：
    1.buffer
        buffer 方法作用是缓冲组合，第一个参数是缓冲时间，第二个参数是缓冲个数，第三个参数是线程。
    2.window
        window 操作符和 buffer 十分相似。不过 buffer 是周期性的将缓存的元素集合发送出来，而 window 周期性的将元素集合以 Observable 的形态发送出来。集合元素，作为一个Observable发出
    3.map
        该操作符通过传入一个函数闭包把原来的 Observable 序列转变为一个新的 Observable 序列。
    4.flatMap
        map 在做转换的时候容易出现“升维”的情况。即转变之后，从一个序列变成了一个序列的序列。
        而 flatMap 操作符会对源 Observable 的每一个元素应用一个转换方法，将他们转换成 Observables。 然后将这些 Observables 的元素合并之后再发送出来。即又将其 "拍扁"（降维）成一个 Observable 序列。
    5.flatMapLatest
        flatMapLatest 与 flatMap 的唯一区别是：flatMapLatest 只会接收最新的 value 事件。
    6.flatMapFirst
        flatMapFirst 与 flatMapLatest 正好相反：flatMapFirst 只会接收最初的 value 事件。
        该操作符可以防止重复请求：
            比如点击一个按钮发送一个请求，当该请求完成前，该按钮点击都不应该继续发送请求。便可该使用 flatMapFirst 操作符。
    7.concatMap
        与 flatMap 的唯一区别是：当前一个 Observable 元素发送完毕后，后一个Observable 才可以开始发出元素。或者说等待前一个 Observable 产生完成事件后，才对后一个 Observable 进行订阅。
    8.scan
        scan 就是先给一个初始化的数，然后不断的拿前一个结果和最新的值进行处理操作。
    9.groupBy
        groupBy 操作符将源 Observable 分解为多个子 Observable，然后将这些子 Observable 发送出来。
        也就是说该操作符会将元素通过某个键进行分组，然后将分组后的元素序列以 Observable 的形态发送出来。
 */
struct Code07ViewModel {
    let data = Observable.just([
        CellInfoModel(title: "变换操作符：buffer", funcName:"buffer"),
        CellInfoModel(title: "变换操作符：window", funcName:"window"),
        CellInfoModel(title: "变换操作符：map", funcName:"map"),
        CellInfoModel(title: "变换操作符：flatMap", funcName:"flatMap"),
        CellInfoModel(title: "变换操作符：flatMapLatest", funcName:"flatMapLatest"),
        CellInfoModel(title: "变换操作符：flatMapFirst", funcName:"flatMapFirst"),
        CellInfoModel(title: "变换操作符：concatMap", funcName:"concatMap"),
        CellInfoModel(title: "变换操作符：scan", funcName:"scan"),
        CellInfoModel(title: "变换操作符：groupBy", funcName:"groupBy"),
        ])
}

class Code07ViewController: UIViewController {

    let CellReuseID = "CellReuseID"
    let viewModel = Code07ViewModel()
    lazy var tableView : UITableView = {
        let tableView = UITableView.init(frame: self.view.bounds, style: .plain)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellReuseID)
        return tableView
    }()
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.addSubview(tableView)
        
        viewModel.data.bind(to: tableView.rx.items(cellIdentifier : CellReuseID)) { _, model , cell in
            cell.textLabel?.text = model.title
            cell.detailTextLabel?.text = model.funcName
            
        }.disposed(by: disposeBag)
        
        tableView.rx.modelSelected(CellInfoModel.self).subscribe(onNext: { model in
            let aSelec = NSSelectorFromString(model.funcName)
            self.performSelector(inBackground: aSelec, with: nil)
            
        }).disposed(by: disposeBag)
        
    }
    
    @objc func buffer() {
        let subject = PublishSubject<String>()
        
        // 每缓存3个元素则组合起来一起发出
        // 如果1秒钟内不够3个也会发出（有几个发几个，一个都没有发空数组[]）
        subject.buffer(timeSpan: 1, count: 3, scheduler: MainScheduler.instance)
            .subscribe(onNext: {print($0)})
            .disposed(by: disposeBag)
        
        subject.onNext("a")
        subject.onNext("b")
        subject.onNext("c")
        
        subject.onNext("1")
        subject.onNext("2")
        subject.onNext("3")
        
    }
    
    @objc func window() {
        let subject =  PublishSubject<String>()
        
        //每3个元素作为一个子Observable发出
        subject.window(timeSpan: 1, count: 3, scheduler: MainScheduler.instance)
            .subscribe(onNext: {[weak self] in
                print("subscribe:\($0)")
                $0.asObservable()
                    .subscribe(onNext: {print($0)})
                    .disposed(by: self!.disposeBag)
            }).disposed(by: disposeBag)
        
        subject.onNext("a")
        subject.onNext("b")
        subject.onNext("c")
        
        subject.onNext("1")
        subject.onNext("2")
        subject.onNext("3")
    }
    
    @objc func map() {
        Observable.of(1, 2, 3)  //原数据
            .map { $0 * 3 }     //变换后的数据
            .subscribe(onNext: {print($0)})
            .disposed(by: disposeBag)
    }
    
    @objc func flatMap() {
        let subject1 = BehaviorSubject(value: "A")
        let subject2 = BehaviorSubject(value: "1")
        
        let variable = Variable(subject1)
        
        variable.asObservable()
            .flatMap{ $0 }
            .subscribe(onNext: { print($0) })
            .disposed(by: disposeBag)
        
        subject1.onNext("B")
        variable.value = subject2
        subject2.onNext("2")
        subject1.onNext("1")
    }
    
    @objc func flatMapLatest() {
        let subject1 = BehaviorSubject(value: "A")
        let subject2 = BehaviorSubject(value: "1")
        
        let variable = Variable(subject1)
        
        variable.asObservable()
            .flatMapLatest{$0}
            .subscribe(onNext: {print($0)})
            .disposed(by: disposeBag)
        
        subject1.onNext("B")
        variable.value = subject2   //接收subject2的信号
        subject2.onNext("2")
        subject1.onNext("C")    //不再接收subject1的信号
        
    }
    
    @objc func flatMapFirst() {
        let subject1 = BehaviorSubject(value: "A")
        let subject2 = BehaviorSubject(value: "1")
        
        let variable = Variable(subject1)
        
        variable.asObservable()
            .flatMapFirst{$0}
            .subscribe(onNext: {print($0)})
            .disposed(by: disposeBag)
        
        subject1.onNext("B")
        variable.value = subject2
        subject2.onNext("2")    //不接收subject2的信号
        subject1.onNext("C")
        
    }
    
    @objc func concatMap() {
        let subject1 = BehaviorSubject(value: "A")
        let subject2 = BehaviorSubject(value: "1")
        
        let variable = Variable(subject1)
        
        variable.asObservable()
            .concatMap{ $0 }
            .subscribe(onNext: {print($0)})
            .disposed(by: disposeBag)
        
        subject1.onNext("B")
        variable.value = subject2
        subject2.onNext("2")
        subject1.onNext("C")
        subject1.onCompleted()  //只有前一个序列subject1结束后，才能接收下一个序列subject2的信号
        
    }
    
    @objc func scan() {
        Observable.of(1, 2, 3, 4, 5)
            .scan(0) {acum, elem in
                acum + elem
        }
            .subscribe(onNext:{print($0)})
            .disposed(by: disposeBag)
    }
    
    @objc func groupBy() {
        //将奇数偶数分成两组
        Observable<Int>.of(0, 1, 2, 3, 4, 5)
            .groupBy(keySelector: { element -> String in
                return element % 2 == 0 ? "偶数" : "奇数"
            })
            .subscribe { [weak self] event in
                switch event {
                case .next(let group):
                    group.asObservable().subscribe( { event in
                        print("key:\(group.key) event:\(event)")
                    })
                        .disposed(by: self!.disposeBag)
                default:
                    print("")
                }
        }
            .disposed(by: disposeBag)
    }
}
